package uz.devops.devops;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DevopsApplication {

	@RequestMapping("/")
	public String home() {
		return "Hello Docker World wtf #2 change docker file";
	}

	public static void main(String[] args) {
		SpringApplication.run(DevopsApplication.class, args);
	}
}
